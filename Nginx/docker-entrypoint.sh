#!/usr/bin/env sh

set -eu

PORT=8443
envsubst '${PORT}' < /etc/nginx/conf.d/default.conf.template > /etc/nginx/conf.d/default.conf

exec "$@"